/**
 * Created by Esraa Ahmed on 24/12/2018.
 */
var d = new Date();


$(document).ready(function (){
    $('#date').html(d.getDate() +"/" + (d.getMonth()+1) +"/"+d.getFullYear())
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

});
