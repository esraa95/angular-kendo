import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardWinComponent } from './dashboard-win.component';

describe('DashboardWinComponent', () => {
  let component: DashboardWinComponent;
  let fixture: ComponentFixture<DashboardWinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
