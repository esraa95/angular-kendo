import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-win',
  templateUrl: './dashboard-win.component.html',
  styleUrls: ['./dashboard-win.component.css']
})
export class DashboardWinComponent implements OnInit {
  @Input('top') topLoc: string;
  @Input('left') leftLoc: string;

  public windowTop: number  ;
  public windowLeft: number ;
  public isDraggable: boolean = false;
  public opened = true;
  public dataSaved = false;

  public close() {
    this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public submit() {
    this.dataSaved = true;
    this.close();
  }
  constructor() { }

  ngOnInit() {
    this.windowTop =  parseInt(this.topLoc);
    this.windowLeft =  parseInt(this.leftLoc);
  }

}

