/**
 * Created by Esraa Ahmed on 25/12/2018.
 */
export const Employees = [
    {
        "EmpName": "Esraa",
        "EmpCode": "123",
        "Gender": "female",
        "DOF": "15/11/1995",

    },
    {
        "EmpName": "Ahmed",
        "EmpCode": "456",
        "Gender": "male",
        "DOF": "20/11/1990",
    },
    {
        "EmpName": "Aly",
        "EmpCode": "825",
        "Gender": "male",
        "DOF": "20/1/1990",
    },
]